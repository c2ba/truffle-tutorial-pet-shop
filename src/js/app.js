class App {
  web3 = null
  adoptionContractInstance = null

  constructor() {
    this.init();
  }

  async init() {
    await this.initWeb3();
    await this.initContract();
    this.bindEvents();
  }

  async initWeb3() {
    if (!window.ethereum) {
      // Only handle modern dapp browsers
      document.getElementById("content").innerHTML("Please install a modern wallet extension.");
      return;
    }
    try {
      await window.ethereum.request({ method: "eth_requestAccounts" });
    } catch (error) {
      document.getElementById("content").innerHTML("Please connect with wallet extension.");
      return;
    }
    this.web3 = new Web3(window.ethereum);
  }

  async initContract() {
    const data = await (await fetch("contracts/Adoption.json")).json();

    const adoptionContract = TruffleContract(data);
    adoptionContract.setProvider(window.ethereum);
    this.adoptionContractInstance = await adoptionContract.deployed()

    await this.drawUI();
  }

  async bindEvents() {
    window.ethereum.on('accountsChanged', async (accounts) => {
      await this.drawUI();
    });
    document.addEventListener(
      "click",
      (e) => {
        if (e.target.matches(".btn-adopt")) {
          this.handleAdopt(e);
        }
        if (e.target.matches(".btn-abandon")) {
          this.handleAbandon(e);
        }
      },
    );
  }

  async drawUI() {
    const adopters = await this.adoptionContractInstance.getAdopters.call()
    const hasOwner = adopters.map(adopter => adopter !== "0x0000000000000000000000000000000000000000");
    const isMine = adopters.map(adopter => adopter === this.web3.eth.accounts[0]);

    const makeButton = (petId) => {
      if (!hasOwner[petId]) {
        return `<button class="btn-adopt" type="button" data-id="${petId}">Adopt 😍</button>`;
      }
      if (isMine[petId]) {
        return `<button class="btn-abandon" type="button" data-id="${petId}">Abandon 😞</button>`;
      }
      return `<button disabled type="button">Don't try to steal a dog ! 😠</button>`;
    }

    const pets = await (await fetch("data/pets.json")).json();

    const petsHtml = "".concat(pets.map((pet, petId) => `
      <div class="panel-pet">
        <div class="panel-heading">
          <h3 class="panel-title">${pet.name}</h3>
        </div>
        <div class="panel-body">
          <img alt="140x140" data-src="holder.js/140x140" class="img-rounded img-center" style="width: 100%;" src="${pet.picture}" data-holder-rendered="true">
          <br/><br/>
          <strong>Breed</strong>: <span class="pet-breed">${pet.breed}</span><br/>
          <strong>Age</strong>: <span class="pet-age">${pet.age}</span><br/>
          <strong>Location</strong>: <span class="pet-location">${pet.location}</span><br/>
          <span class="panel-owner ${hasOwner[petId] ? '' : 'hidden'}"><strong>Owner</strong>: <span class="pet-owner">${adopters[petId]}</span></span><br/><br/>
          ${makeButton(petId)}
        </div>
      </div>
    `));

    document.getElementById("content").innerHTML = `<div id="pets">${petsHtml}</div>`
  }

  async handleAdopt(event) {
    event.preventDefault();

    const petId = parseInt(event.target.dataset["id"]);

    await this.adoptionContractInstance.adopt(petId, {from: this.web3.eth.accounts[0]});
    await this.drawUI();
  }

  async handleAbandon(event) {
    event.preventDefault();

    const petId = parseInt(event.target.dataset["id"]);

    await this.adoptionContractInstance.abandon(petId, {from: this.web3.eth.accounts[0]});
    await this.drawUI();
  }
};

(()  => {
  window.addEventListener("load", () => {
    new App();
  });
})();
