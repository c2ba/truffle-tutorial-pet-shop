pragma solidity >=0.4.22 <0.8.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Adoption.sol";

contract TestAdoption {
    Adoption adoption = Adoption(DeployedAddresses.Adoption());

    uint256 expectedPetId = 8;
    address expectedAdopter = address(this);

    function testUserCanAdoptPet() public {
        uint256 petId = adoption.adopt(expectedPetId);
        Assert.equal(
            petId,
            expectedPetId,
            "Adoption of the expected pet should match what is returned"
        );
    }

    function testGetAdopterAddressByPetId() public {
        address adopter = adoption.adopters(expectedPetId);

        Assert.equal(
            adopter,
            expectedAdopter,
            "Owner of the expected pet should be this contract"
        );
    }

    function testGetAdopterAddressByPetIdInArray() public {
        address[16] memory adopters = adoption.getAdopters();

        Assert.equal(
            adopters[expectedPetId],
            expectedAdopter,
            "Owner in array of the expected pet should be this contract"
        );
    }

    function testOwnerCanAbandonPet() public {
        uint256 petId = adoption.abandon(expectedPetId);
        Assert.equal(
            petId,
            expectedPetId,
            "Abandon of the expected pet should match what is returned"
        );
        address newAdopter = adoption.adopters(expectedPetId);
        Assert.equal(
            newAdopter,
            address(0),
            "Abandonned pet should have no owner"
        );
    }
}
