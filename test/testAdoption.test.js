const Adoption = artifacts.require("Adoption");

contract("Adoption", (accounts) => {
  let adoption;
  const expectedPetId = 8;

  before(async () => {
    adoption = await Adoption.deployed();
  });

  describe("adopting a pet and retrieving account addreses", async () => {
    let expectedAdopter;
    before("adopt a pet using accounts[0]", async () => {
      await adoption.adopt(expectedPetId, { from: accounts[0] });
      expectedAdopter = accounts[0];
    })

    it("can fetch the address of an owner by pet id", async () => {
      const adopter = await adoption.adopters(expectedPetId);
      assert.equal(adopter, expectedAdopter, "The owner of the adopted pet should be the first account.");
    });

    it("can fetch the collection of all pet owners' address", async () => {
      const adopters = await adoption.getAdopters();
      assert.equal(adopters[8], expectedAdopter, "The owner of the adopted pet in array should be the first account.");
    });

    it("can abandon the pet", async() => {
      await adoption.abandon(expectedPetId, { from: accounts[0] });
      const adopter = await adoption.adopters(expectedPetId);
      assert.equal(adopter, "0x0000000000000000000000000000000000000000", "The abandonned pet should have no owner");
    });

    it("cannot abandon the pet of someone else", async () => {
      expect(async () => {
        await adoption.abandon(expectedPetId, { from: accounts[1] });
      }).to.throw
    });
  })
});
